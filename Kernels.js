class Kernels{
	constructor(interaction_radius_h){
		this.PI = Math.PI;
		this.H = interaction_radius_h;	//h
		this.H_SQU = this.H*this.H; 	//h²
		this.H_3 = this.H*this.H*this.H;//h³
		
		// see 3.5 Smoothing Kernels in SPH Paper
		// it is only needed to calculate this term once
		// (h² - r²)³ is needed to calculate in the method Poly6Kernel
		this.POLY6_TERM = (315.0 / (64.0 * this.PI * Math.pow(this.H,9)));
		this.SPIKY_TERM = (-45.0/(this.PI * Math.pow(this.H,6)));
	}
	// Used Kernels for PBD:
	// Poly6Kernel for density estimation
	// SpikyKernel for gradient calculation
	// For more information see: http://mmacklin.com/pbf_sig_preprint.pdf section 3 "Enforcing Incrompressibility"
	// And original SPH Paper: http://matthias-mueller-fischer.ch/publications/sca03.pdf
	
	// Create SPH-Related Tutorial about Vector-Calculus (Gradient,Divergence,Laplacian etc.) and general SPH techniques
	// in the Book "Fluid Engine Development" from Doyub Kim
	
	// Wpoly6(r,h) see 3.5 Smoothing Kernels in SPH Paper
	// pi = Particle position of index i and pj = neighbour particle position
	Poly6Kernel(pi,pj){
		let r = Sub(pi,pj);
		let length2 = r.Length2();
		if(length2 > this.H_SQU || length2 <= 0){
			return 0;
		}else{
			let length = Math.sqrt(length2);
			return (this.POLY6_TERM * Math.pow((this.H*this.H - length2),3)); 
		}
	}
	
	// Gradient is the first derivative of the SpikyKernel multiplied by a direction vector
	SpikyGradient(pi,pj){
		let r = Sub(pi,pj);
		let length2 = r.Length2();
		if(length2 > this.H_SQU || length2 <= 0){
			return new Vector2(0,0);
		}else{
			let length = Math.sqrt(length2);
			r.Normalize();
			let term = (this.H - length) * (this.H - length) * this.SPIKY_TERM;
			return Scale(r,term);
		}			
	}
}